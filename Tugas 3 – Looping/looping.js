// No. 1 Looping While
console.log('LOOPING PERTAMA')
var i = 2
while ( i < 21) {
    if(i%2==0){
    console.log(i+" - I love coding")
    }
    i++
}
console.log('LOOPING KEDUA')
var i = 20
while (i > 1) {
    if(i%2==0){
    console.log(i+" - I will become web developer")
    }
    i--
}

// No. 2 Looping menggunakan for
console.log('OUTPUT')
for (var i = 1; i < 21; i++) {
    if(i%3==0 && i%2==1){
    console.log(i+" - I Love Coding")
    }else if(i%2==1){
        console.log(i+" - Santai")
    }else if(i%2==0){
        console.log(i+" - Berkualitas")
    }
}

// No. 3 Membuat Persegi Panjang 
console.log('Persegi Panjang')
var y=''
for (let i = 0; i < 4; i++) {
    for (var x = 0; x < 8; x++)
    {
      if (x == 7) {
        y += "\n";
      } else{
        y += "*";
      }
    }
}
console.log(y)
    
// No. 4 Membuat Tangga
console.log('Tangga')
var x = ''
for(var i=1; i <= 7; i++)
 {
  for( var j=1; j<=i; j++)
 {
    x += '*';
  }
   console.log(x);
   var x= ''
 }

//  No. 5 Membuat Papan Catur
console.log('papan catur')
height =8
width = 9
board=''
for (var y = 0; y < height; y++) {   
   for (var x = 0; x < width; x++)
   {
     if (x == width - 1) {
       board += "\n";
     } else if ((x + y) % 2 == 0){
       board += " ";
     }
     else {
       board += "#";
     }
   }
}
console.log(board)