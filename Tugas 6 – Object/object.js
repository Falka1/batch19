// Soal No. 1 (Array to Object)

function arrayToObject(array) {
   for (let i = 0; i < array.length; i++) {
       var obj ={}
       var thnLahir = array[i][3]
       var thnSekarang = new Date().getFullYear()
       if (thnSekarang > thnLahir) {
           umur = thnSekarang - thnLahir        
       } else {
           umur = "Invalid Birth Year"
       }
       obj.Firstname = array[i][0]
       obj.Lastname = array[i][1]
       obj.Gender = array [i][2]
       obj.Age = umur
       

    console.log(`${i+1}. ${obj.Firstname} ${obj.Lastname} :`,obj)
       
   }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

// Soal No. 2 (Shopping Time)
function shoppingTime(memberId, money) {
    if (!memberId) {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    } else if(money < 50000) {
        return 'Mohon maaf, uang tidak cukup'
    } else {
        var obj = {}
        obj.memberId = memberId
        obj.money = money
        var listPurchased=[]
        if(money >= 50000){
            if(money >= 1500000 ){
                money = money - 1500000
                listPurchased.push('Sepatu Stacattu')
            }
            if(money >= 500000){
                money = money - 500000
                listPurchased.push('Baju Zoro')
            }
            if(money >= 250000){
                money = money - 250000
                listPurchased.push('Baju H&N')
            }
            if(money >= 175000){
                money = money - 175000
                listPurchased.push('Baju Uniklooh')
            }
            if(money >= 50000 || listPurchased.includes('Casing Handphone') == true){
                money = money - 50000
                listPurchased.push('Casing Handphone')
            }
        var sisa = money
        
        }
        obj.listPurchased =listPurchased
        obj.changeMoney = sisa



        return obj
    }
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); //Mohon maaf, toko X hanya berlaku untuk member saja

//   Soal No. 3 (Naik Angkot)
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var arr=[]
    for (let i = 0; i < arrPenumpang.length; i++) {
        obj={}
        obj.penumpang = arrPenumpang[i][0]
        obj.naikDari = arrPenumpang[i][1]
        obj.tujuan = arrPenumpang[i][2]
        obj.bayar = (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1]))*2000
        arr.push(obj)
    }
    return arr
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]