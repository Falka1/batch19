// Soal No. 1 (Range)
console.log("Soal No. 1 (Range)")
function range(start, end) {
    var ans = [];
    if(!start || !end){
         ans = -1 
        
    } else if (start < end) {
        for (let i = start; i <= end; i++) {
            ans.push(i);
        }
    } else if(start > end){
        for (let i = start; i >= end; i--) {

            ans.push(i);
        }
    }
    return ans;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 




// Soal No. 2 (Range with Step)
console.log("Soal No. 2 (Range with Step)")
function rangeWithStep(start, end, step) {
    var ans = [];
    var cur = start
    if (start < end) {
        for (let i = 0; cur <= end; i++) {
            ans.push(cur);
            cur +=step
        }
    } else if(start > end){
        for (let i = 0; cur >= end; i++) {
            ans.push(cur);
            cur-=step
        }
    }
    return ans;
}


console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


// Soal No. 3 (Sum of Range)
console.log('Soal No. 3 (Sum of Range)')
function sum(start, end, step) {
    var ans = [];
    var cur = start

    if(!step){
        step=1
    }
    if (start < end) {
        for (let i = 0; cur <= end; i++) {
            ans.push(cur);
            cur +=step
        }
    } else if(start > end){
        for (let i = 0; cur >= end; i++) {
            ans.push(cur);
            cur-=step
        }
    }else if (!end){
        ans.push(start)
    }else if(!start && !end){
        ans.push(0)
    }
    var total = 0;

    for (var i  = 0; i < ans.length; i++){
 
       total  += ans[i];
 
    };
    return total
}


console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


// Soal No. 4 (Array Multidimensi)
console.log("Soal No. 4 (Array Multidimensi)")
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(array){
    var newString=''
    for (let i = 0; i < array.length; i++) {
        newString+= `Nomor ID:  ${array[i][0]}\n`;
        newString+= 'Nama Lengkap: ' + array[i][1]+'\n';
        newString+= 'TTL: ' + array[i][2]+' '+array[i][3] +'\n';
        newString+= 'Hobi: ' + array[i][4]+'\n\n';
        
    }
    return newString
}

console.log(dataHandling(input))

// Soal No. 5 (Balik Kata)
console.log("Soal No. 5 (Balik Kata)")

function balikKata(word) {
    var newString = "";
    for (var i = word.length - 1; i >= 0; i--) {
        newString += word[i];
    }
    return newString;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Soal No. 6 (Metode Array)
console.log("Soal No. 6 (Metode Array)")
function dataHandling2(array) {
   array.splice(1,2,`${array[1]}Elsharawy`,`Provinsi ${array[2]}`)
   array.splice(4,1,"Pria","SMA Internasional Metro")
   console.log(array)

   var date = array[3].split("/")
   switch(date[1]) {
    case '01':
        month = "Januari";
        break;
    case '02':
        month = "Februari";
        break;
    case '03':
        month = "Maret";
        break;
    case '04':
        month = "April";
        break;
    case '05':
        month = "Mei";
        break;
    case '06':
        month = "Juni";
        break;
    case '07':
        month = "July";
        break;
    case '08':
        month = "Agustus";
        break;
    case '09':
        month = "September";
        break;
    case '10':
        month = "Oktober";
        break;
    case '11':
        month = "November";
        break;
    case '12':
        month = "Desember";
        break;
    default:
      month = "(Bulan antara 1-12!)";
   }
    console.log(month)

    console.log(date.sort(function(a, b){return b - a}))
    console.log(array[1].slice(0,15))
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 
 